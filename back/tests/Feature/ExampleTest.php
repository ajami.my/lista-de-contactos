<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class ContactListTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  contact_list (
                id	TEXT NOT NULL,
                name	TEXT,
                familyName	TEXT,
                mobile	TEXT NOT NULL,
                email	TEXT
            );
            INSERT INTO contact_list VALUES ('01','yassin', 'ajami','000000','test@gmail.com');
            INSERT INTO contact_list VALUES ('02','amjad', 'ajami','111111','zzzz@gmail.com');
        ");
         //$this->withoutExceptionHandling();
    }

    public function testGetContactlist()
    {
        $this->json('GET', 'api/contact_list')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id' => '02',
                    'name' => 'amjad',
                    'familyName' => 'ajami',
                    'mobile' => '111111',
                    'email' => 'zzzz@gmail.com',
                ],
                [
                    'id' => '01',
                    'name' => 'yassin',
                    'familyName' => 'ajami',
                    'mobile' => '000000',
                    'email' => 'test@gmail.com',
                ],
            ]);
    }

     public function testGetContact()
    {
        $this->json('GET', 'api/contact_list/02')
             ->assertStatus(200)
             ->assertJson(
                 [
                     'id' => '02',
                    'name' => 'amjad',
                    'familyName' => 'ajami',
                    'mobile' => '111111',
                    'email' => 'zzzz@gmail.com',
                 ]
             );
     }

     public function testPostContact()
     {
         $this->json('POST', 'api/contact_list', [
                    'id' => '03',
                    'name' => 'nicky',
                    'familyName' => 'lazal',
                    'mobile' => '3456789',
                    'email' => 'lazal@gmail.com',
         ])->assertStatus(200);

         $this->json('GET', 'api/contact_list/03')
             ->assertStatus(200)
             ->assertJson([
                'id' => '03',
                    'name' => 'nicky',
                    'familyName' => 'lazal',
                    'mobile' => '3456789',
                    'email' => 'lazal@gmail.com',
             ]);
     }

     public function testPut()
     {
         $data = [
                    'id' => '01',
                    'name' => 'yassinx',
                    'familyName' => 'ajami',
                    'mobile' => '000000',
                    'email' => 'test@gmail.com',
         ];

         $expected = [
             'id' => '01',
                    'name' => 'yassinx',
                    'familyName' => 'ajami',
                    'mobile' => '000000',
                    'email' => 'test@gmail.com',
         ];

         $this->json('PUT', 'api/contact_list/01', $data)
             ->assertStatus(200)
             ->assertJson($expected);

        $this->json('GET', 'api/contact_list/01')
             ->assertStatus(200)
             ->assertJson($expected);
     }

     public function testPatch()
     {
         $this->json('PATCH', 'api/contact_list/01', [
                   'name' => 'yassin',
         ])
             ->assertStatus(200)
            ->assertJson([
                 'id' => '01',
                    'name' => 'yassin',
                    'familyName' => 'ajami',
                    'mobile' => '000000',
                    'email' => 'test@gmail.com',
             ]);

        $this->json('GET', 'api/contact_list/01')
             ->assertStatus(200)
             ->assertJson([
                 'name' => 'yassin',
             ]);

         $this->json('PATCH', 'api/contact_list/01', [
            'familyName' => 'ajami',
                    'mobile' => '000333',
                    'email' => 'yassine@gmail.com',
         ])
             ->assertStatus(200)
            ->assertJson([
                 'id' => '01',
                    'name' => 'yassin',
                    'familyName' => 'ajami',
                    'mobile' => '000333',
                    'email' => 'yassine@gmail.com',
             ]);

         $this->json('GET', 'api/contact_list/01')
             ->assertStatus(200)
             ->assertJson([
                 'id' => '01',
                    'name' => 'yassin',
                    'familyName' => 'ajami',
                    'mobile' => '000333',
                    'email' => 'yassine@gmail.com',
             ]);
     }

     public function testDeleteContact()
     {
         $this->json('GET', 'api/contact_list/01')->assertStatus(200);

         $this->json('DELETE', 'api/contact_list/01')->assertStatus(200);

         $this->json('GET', 'api/cacontact_listrs/01')->assertStatus(404);
     }

     public function testGetContactNotExist()
     {
          $this->json('GET', 'api/contact_list/zzz')->assertStatus(404);

         $this->json('DELETE', 'api/contact_list/zzz')->assertStatus(404);

        $this->json('PUT', 'api/contact_list/zzz', 
            [
                'id' => 'zzz',
                    'name' => 'yassin',
                    'familyName' => 'ajami',
                    'mobile' => '000333',
                    'email' => 'yassine@gmail.com',
            ]
        )
            ->assertStatus(404);

         $this->json('PATCH', 'api/contact_list/zzz', [
             'name' => 'yassin',
         ])
            ->assertStatus(404);
     }

}



class CommentsTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  comments (
                id	TEXT NOT NULL,
                idContact	TEXT,
                commentDate	TEXT,
                commentDetails	TEXT NOT NULL
            );
            INSERT INTO comments VALUES ('01','932d2ead-7c7c-4256-951c-2c6bff1312de', '12/12/2020','comment test 1');
            INSERT INTO comments VALUES ('02','932d2ead-7c7c-4256-951c-2c6bff1312de', '12/12/2020','comment test 2');
            INSERT INTO comments VALUES ('03','1d043ebf-f772-4a81-9f8f-5c2b8e677adb', '12/12/2020','comment test 3');
        ");
         //$this->withoutExceptionHandling();
    }

    
     public function testGetContactComments()
    {
        $this->json('GET', 'api/comments')
             ->assertStatus(200)
             ->assertJson([
                 [
                     'id' => '01',
                    'idContact' => '932d2ead-7c7c-4256-951c-2c6bff1312de',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 1',
                   
                 ],
                 [
                     'id' => '02',
                    'idContact' => '932d2ead-7c7c-4256-951c-2c6bff1312de',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 2',
                   
                 ],
                 [
                     'id' => '03',
                    'idContact' => '1d043ebf-f772-4a81-9f8f-5c2b8e677adb',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 3',
                   
                 ],
             ]);
     }

     public function testPostComment()
     {
         $this->json('POST', 'api/comments', [
                     'id' => '04',
                    'idContact' => '1d043ebf-f772-4a81-9f8f-5c2b8e677adb',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 3',
         ])->assertStatus(200);

         $this->json('GET', 'api/comments/04')
             ->assertStatus(200)
             ->assertJson([
                    'id' => '04',
                    'idContact' => '1d043ebf-f772-4a81-9f8f-5c2b8e677adb',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 3',
             ]);
     }

     public function testPut()
     {
         $data = [
                    'id' => '03',
                    'idContact' => '1d043ebf-f772-4a81-9f8f-5c2b8e677adb',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 3',
         ];

         $expected = [
             'id' => '03',
                    'idContact' => '1d043ebf-f772-4a81-9f8f-5c2b8e677adb',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 3',
         ];

         $this->json('PUT', 'api/comments/03', $data)
             ->assertStatus(200)
             ->assertJson($expected);

        $this->json('GET', 'api/comments/03')
             ->assertStatus(200)
             ->assertJson($expected);
     }

    //  public function testPatch()
    //  {
    //      $this->json('PATCH', 'api/comments/03', [
    //                 'commentDate' => '12/12/xxxx',
    //      ])
    //          ->assertStatus(200)
    //         ->assertJson([
    //              'id' => '01',
    //                 'name' => 'yassin',
    //                 'familyName' => 'ajami',
    //                 'mobile' => '000000',
    //                 'email' => 'test@gmail.com',
    //          ]);

    //     $this->json('GET', 'api/contact_list/01')
    //          ->assertStatus(200)
    //          ->assertJson([
    //              'name' => 'yassin',
    //          ]);

    //      $this->json('PATCH', 'api/contact_list/01', [
    //         'familyName' => 'ajami',
    //                 'mobile' => '000333',
    //                 'email' => 'yassine@gmail.com',
    //      ])
    //          ->assertStatus(200)
    //         ->assertJson([
    //              'id' => '01',
    //                 'name' => 'yassin',
    //                 'familyName' => 'ajami',
    //                 'mobile' => '000333',
    //                 'email' => 'yassine@gmail.com',
    //          ]);

    //      $this->json('GET', 'api/contact_list/01')
    //          ->assertStatus(200)
    //          ->assertJson([
    //              'id' => '01',
    //                 'name' => 'yassin',
    //                 'familyName' => 'ajami',
    //                 'mobile' => '000333',
    //                 'email' => 'yassine@gmail.com',
    //          ]);
    //  }

     public function testDeleteContact()
     {
         $this->json('GET', 'api/comments/01')->assertStatus(200);

         $this->json('DELETE', 'api/comments/01')->assertStatus(200);

         $this->json('GET', 'api/comments/01')->assertStatus(404);
     }

     public function testGetContactNotExist()
     {
          $this->json('GET', 'api/comments/zzz')->assertStatus(404);

         $this->json('DELETE', 'api/comments/zzz')->assertStatus(404);

        $this->json('PUT', 'api/comments/zzz', 
            [
                'id' => 'zzz',
                    'idContact' => '932d2ead-7c7c-4256-951c-2c6bff1312de',
                    'commentDate' => '12/12/2020',
                    'commentDetails' => 'comment test 1',
                   
            ]
        )
            ->assertStatus(404);

         $this->json('PATCH', 'api/comments/zzz', [
            'commentDetails' => ' patch comment test 2',
         ])
            ->assertStatus(404);
     }

}   
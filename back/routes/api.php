<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/contact_list', function (Request $request) {
     //contact_list (id, name, familyName, mobile, email)  a cambiar *
    $results = DB::select('select * from contact_list ORDER BY name');
    return response()->json($results, 200);
});
Route::get('/search_contacts/{txt}', function ($txt) {
     //contact_list (id, name, familyName, mobile, email)  a cambiar *
    $results = DB::select('select * from contact_list WHERE name LIKE "%' . $txt . '%"');
    return response()->json($results, 200);
});

Route::get('/contact_list/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (contactNoExists($id)) {
        abort(404);
    }

    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from contact_list where id=:id ', [
        'id' => $id,
    ]);

    // Así se devuelve un JSON.
    // El primer parámetro es el dato que se manda como JSON.
    // El segundo parámetro es el status code
    return response()->json($results[0], 200);
});

Route::post('/contact_list/', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into contact_list (id, name, familyName, mobile, email)
        values (:id, :name, :familyName, :mobile, :email)
    ",
        $data
    );

    $results = DB::select('select * from contact_list where id = :id', [
        'id' => $data['id'],
    ]);
    return response()->json($results[0], 200);
});

Route::put('/contact_list/{id}', function ($id) {
    if (contactNoExists($id)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from contact_list where id = :id",
        ['id' => $id]
    );

    DB::insert(
        "
        insert into contact_list (id, name, familyName, mobile, email)
        values (:id, :name, :familyName, :mobile, :email)
    ",
        $data
    );

    $results = DB::select('select * from contact_list where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::patch('/contact_list/{id}', function ($id) {
    if (contactNoExists($id)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update contact_list SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );

    $results = DB::select('select * from contact_list where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::delete('/contact_list/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (contactNoExists($id)) {
        abort(404);
    }

    DB::delete('delete from contact_list where id = :id', ['id' => $id]);

    return response()->json('', 200);
});

// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('contactNoExists')) {
    function contactNoExists($id)
    {
        $results = DB::select('select * from contact_list where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}

Route::get('/comments', function (Request $request) {
     //contact_list (id, name, familyName, mobile, email)  a cambiar *
    $results = DB::select('select * from comments ');
    return response()->json($results, 200);
});  



Route::get('/comments/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (commentNoExists($id)) {
        abort(404);
    }

    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from comments where id=:id', [
        'id' => $id,
    ]);

    // Así se devuelve un JSON.
    // El primer parámetro es el dato que se manda como JSON.
    // El segundo parámetro es el status code
    return response()->json($results[0], 200);
});


Route::post('/comments/', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into comments (id, idContact, commentDate, commentDetails)
        values (:id, :idContact, :commentDate, :commentDetails)
    ",
        $data
    );

    $results = DB::select('select * from comments where id = :id', [
        'id' => $data['id'],
    ]);
    return response()->json($results[0], 200);
});

Route::put('/comments/{id}', function ($id) {
    if (commentNoExists($id)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from comments where id = :id",
        ['id' => $id]
    );

    DB::insert(
        "
        insert into comments (id, idContact, commentDate, commentDetails)
        values (:id, :idContact, :commentDate, :commentDetails)
    ",
        $data
    );

    $results = DB::select('select * from comments where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::patch('/comments/{id}', function ($id) {
    if (commentNoExists($id)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update comments SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );

    $results = DB::select('select * from comments where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::delete('/comments/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (commentNoExists($id)) {
        abort(404);
    }

    DB::delete('delete from comments where id = :id', ['id' => $id]);

    return response()->json('', 200);
});

// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('commentNoExists')) {
    function commentNoExists($id)
    {
        $results = DB::select('select * from comments where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}

import { shallowMount } from "@vue/test-utils";
import EditContact from "../../src/components/EditContact.vue";
test("button cancel starting event 'goBack'", async () => {
  const wrapper = shallowMount(EditContact, {
    propsData: {
      contact: {}
    }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const buttonCancel = wrapper.find(".cancel");
  buttonCancel.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["go-back-to-contact-details-event"].length).toBe(1);
  expect(wrapper.emitted()["go-back-to-contact-details-event"][0]).toEqual([]);
});

test("button save starting event 'saveInfo'", async () => {
  const wrapper = shallowMount(EditContact, {
    propsData: {
      contact: []
    }
  });

  expect(wrapper.emitted().click).toBe(undefined);
  const buttonSave = wrapper.find(".save");
  buttonSave.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["save-contac-tevent"].length).toBe(1);
  expect(wrapper.emitted()["save-contac-tevent"][0]).toEqual([[]]);
});

test("comprobamos que data que esta en LocalContact es la misma que esta en Contact de Padre", async () => {
  const wrapper = shallowMount(EditContact, {
    propsData: {
      contact: {
        id: "0919",
        name: "Thanos ",
        familyName: "zanpi11",
        mobile: "69999999",
        email: "zanpi@gmail.com"
      }
    },
    data() {
      return {
        localContact: {}
      };
    },
    watch: {
      contact: {
        immediate: true,
        handler: function() {
          this.localContact = JSON.parse(JSON.stringify(this.contact));
        }
      }
    }
  });
  expect(wrapper.vm.localContact).toEqual(wrapper.vm.contact);
});
/*
  methods: {
    
    saveEditContactInfo(){
       this.$emit("funcionalidadButtonSave",);
    },
    goBack(){
       this.$emit("funcionalidadButtonCancel",);
    }
  },
}; */

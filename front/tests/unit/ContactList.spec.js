import { shallowMount } from "@vue/test-utils";
import ContactList from "@/components/ContactList.vue";
import ContactCard from "@/components/ContactCard.vue";

test("pinta todo list vacío", () => {
  const wrapper = shallowMount(ContactList, {
    propsData: {
      details: []
    }
  });
  const contactCard = wrapper.findAll(ContactCard).wrappers;

  expect(contactCard.length).toBe(0);
});

test("pinta todo los contactos", () => {
  const details = [
    {
      id: "01",
      nombre: "yassin",
      apellido: "ajami",
      movil: "631125578",
      correo: "ajami.my@gmail.com"
    },
    {
      id: "02",
      nombre: "yeray",
      apellido: "espinosa",
      movil: "63254789",
      correo: "yeray.esp@gmail.com"
    }
  ];
  const wrapper = shallowMount(ContactList, {
    propsData: {
      listOfContact: details
    }
  });
  const contactCard = wrapper.findAll(ContactCard).wrappers;
  expect(contactCard.length).toBe(2);
});

test("emite evento button add new contact", async () => {
  const wrapper = shallowMount(ContactList, {
    propsData: {
      details: []
    }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const div = wrapper.find(".add-contact");
  div.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()['go-to-add-contact-event'].length).toBe(1);
  expect(wrapper.emitted()['go-to-add-contact-event'][0]).toEqual([]);
});



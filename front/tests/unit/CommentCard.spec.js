import { shallowMount } from "@vue/test-utils";
import CommentCard from "@/components/CommentCard.vue";

test("pinta en la pantalla", () => {
  const wrapper = shallowMount(CommentCard, {
    propsData: {
      detailsComment: {
        id: "111",
        idContact: "yassine",
        commentDate: "12/12/2020",
        commentDetails: "test test test"
      }
    }
  });

  expect(wrapper.text()).toContain("12/12/2020");
  expect(wrapper.text()).toContain("test test test");
});

test("emite event delete click", async () => {
  const comment = {
    id: "111"
  };
  const wrapper = shallowMount(CommentCard, {
    propsData: { detailsComment: comment }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const deletbutton = wrapper.find(".delete");
  deletbutton.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()["delete-comment-event"].length).toBe(1);
  expect(wrapper.emitted()["delete-comment-event"][0]).toEqual(["111"]);
});
test("emite event seemore click", async () => {
  const comment = {
    id: "111"
  };
  const wrapper = shallowMount(CommentCard, {
    propsData: { detailsComment: comment }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const seemorebutton = wrapper.find(".details");
  seemorebutton.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()["edit-comment-event"].length).toBe(1);
  expect(wrapper.emitted()["edit-comment-event"][0]).toEqual(["111"]);
});

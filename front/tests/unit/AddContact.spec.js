import { shallowMount } from "@vue/test-utils";
import AddContact from "@/components/AddContact.vue";

test("emite evento click", async () => {
  const wrapper = shallowMount(AddContact);
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.

  const input1 = wrapper.find("#name-input");
  const input2 = wrapper.find("#sur-name");
  const input3 = wrapper.find("#phone-number");
  const input4 = wrapper.find("#email-contact");
  const submit = wrapper.find(".delete");
  const exit = wrapper.find(".return");

  input1.setValue("yeray");
  input2.setValue("espinoza");
  input3.setValue("63254789");
  input4.setValue("yeray.esp@gmail.com");
  wrapper.vm.screenState = "AddContactScreen";

  submit.trigger("click");
  await wrapper.vm.$nextTick();
  console.log(wrapper.emitted());
  expect(wrapper.emitted()["save-contact-event"].length).toBe(1);
  expect(wrapper.emitted()["save-contact-event"]).toEqual([
    [
      {
        name: "yeray",
        familyName: "espinoza",
        mobile: "63254789",
        email: "yeray.esp@gmail.com"
      }
    ]
  ]);
});
